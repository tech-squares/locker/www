# allow SSI includes on all .html files.
AddHandler server-parsed .html

# the XHTML1.1 recommendation says that we should serve our pages as
# application/xhtml+xml, instead of text/html.  We'd use:

#AddType application/xhtml+xml;charset=iso-8859-1;lang=en .html

# to do this.  However, not all user agents know what to do with this
# mime type.  The proper thing to do is to use content negotiation to
# return application/xhtml+xml *IF* the user agent sends an Accepts:
# application/xhtml+xml header.
#
# We can do this with apache rewrite rules, but Athena's web server
# doesn't enable these.  We can sorta do it with mod_negotiation
# (which *is* enabled) but it's very ugly: we have to change all links
# from foo.html to foo.<some other suffix>; associate that other suffix
# with a type map with:

#AddHandler type-map .FOO  # where FOO is <some other suffix>

# make symlinks from foo.xhtml to foo.html for all files, change the
# DirectoryIndex to index.<some suffix>, and then set up a foo.FOO file
# for every page on the site with contents like:

#URI: index
#
#URI: index.xhtml
#Content-Type: application/xhtml+xml;charset=iso-8859-1;level=2;qs=1.0
#Content-Language: en
#
#URI: index.html
#Content-Type: text/html;charset=iso-8859-1;qs=0.1
#Content-Language: en

# Yuck!
#
# Using the MultiViews option in apache would save us the trouble of creating
# all those .FOO files, but it doesn't look like Athena will let us turn it
# on.

# Alternatively, we could serve the majority of our pages from scripts.mit.edu
# through some scripty little thing that let us munge the Content-Type, but
# I don't think we want that, either.

# So, at the moment we're going to bow to the present-day realities of Athena
# and serve all our files as text/html.  I tried, I really did.
#                            -- C. Scott Ananian, 20 June 2006, cananian@alum
#------------------------------------------------------------------

Redirect temp /~tech-squares/bylaws https://www.mit.edu/~tech-squares/govdocs
Redirect temp /~tech-squares/bylaws-2015 https://www.mit.edu/~tech-squares/govdocs
Redirect temp /~tech-squares/math https://doi.org/10.1007/s00283-021-10151-0

# Redirect references to old spotlight.html page
<Files spotlight.html>
AddHandler send-as-is .html
</Files>
