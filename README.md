## Introduction
This repository contains the code for the Tech Squares website. 

### Contents
[Updating class timing and meeting locations](#class-timing)  
[Updating call definitions and class handouts](#class-handouts)  
[Updating governing docs](#gov-docs)  
[Server-side includes](#ssi)  
[Archival files](#archival)  
[Reporting issues](#issues)  
  
## <a name="class-timing"></a>Updating class timing and meeting locations



Updates about class timing and meeting locations are frequently required and can be done straighforwardly by altering two html files in this repository.

| File                     | Description                                      | Update                                                                       | Person responsible  |
|--------------------------|--------------------------------------------------|------------------------------------------------------------------------------|---------------------|
| *variables.html*           | Information about when the next class will start | ~2 weeks after a class starts                                                | Webmaster           |
| *schedules/weekly-yy.html* | Event dates and locations.                       | When rooms are booked (~3 times per year) and when room bookings change. | Rooming coordinator |

## <a name="class-handouts"></a>Updating call definitions and class handouts
The .tex files that generate the governing docs are stored in a separate git repository, https://gitlab.com/tech-squares/locker/doc, which has a local clone in the Tech Squares Athena locker. If you wish to update them via Athena, take the following steps:
1. SSH to Athena at athena.dialup.mit.edu.
2. Navigate to /mit/tech-squares/doc/lessons.
3. Update the relevant .tex files and save.
4. Run 'make' to create the new/updated pdf(s) and HTML files in /mit/tech-squares/doc/lessons, and check that they look correct. 
5. Run 'make install' to add the new/updated pdfs to the Tech Squares website repository.
6. Commit your changes to /mit/tech-squares/doc/lessons and push to the git repository.

## <a name="gov-docs"></a>Updating governing docs
The .tex files that generate the governing docs are stored in a separate git repository, https://gitlab.com/tech-squares/locker/doc, which has a local clone in the Tech Squares Athena locker. If you wish to update them via Athena, take the following steps:
1. SSH to Athena at athena.dialup.mit.edu.
2. Navigate to /mit/tech-squares/doc/bylaws.
3. Update the relevant .tex files and save.
4. Run 'make install-stage' to generate the new/updated pdfs and put them in www/govdocs-stage. Take a look at this directory and check that the pdfs you just made look correct.
5. Run 'make install' to add the new/updated pdfs to www/govdocs which will make them live on the website.
6. Commit your changes to /mit/tech-squares/doc/bylaws and push to the git repository.

## <a name="ssi"></a>Server Side Includes
Server Side Includes (SSI) is a set of directives included in HTML files and processed by the web server. They can be used to include the contents of one or more files on a web page. We use SSI on the Tech Squares website to include certain pieces of code that are commonly used throughout the site. 
### Class information
*variables.html* includes information about the timing, location, cost, and PE credit available for the upcoming Tech Squares class. The use of SSI allows the values in this file to be used in the pages *beginners.html* and *announce.html*.
### Headers and footers
SSI is used to populate header and footer information throughout the site. This header and footer information is stored in *head-pretitle.html*, *head-posttitle.html*, *head-posttitle-subdir.html*, and *footer.html*.
### Last modified time
SSI is used to include the last modified time from *lastmod.html* on pages throughout the site. *lastmod.html* uses SSI to get the last modified time for a given page.

## <a name="archival"></a>Archival files
Some of the files in this repository are retained for historical purposes but are not intended to be public-facing. 
### Schedules
The repository contains a *schedules/archive* directory with weekly schedules going back to 2005. As weekly schedules become no longer current, they are moved from the *schedules* directory to *schedules/archive*. 
### Lessons
The repository contains a *lessons* directory with current class material that is intended to be public-facing. In addition, the repo contains three directories with previous iterations of class material that are retained only for historical purposes. These drectories are:
* *lessons-1995*
* *lessons-1998*
* *lessons-2013*
### 50th Anniversary
The repository contains a *50th* directory with pages that were created for the club's 50th Anniversary, which took place in 2017.

## <a name="issues"></a>Reporting issues
Feel free to email the Tech Squares officers or report issues via the GitLab issue tracker. If you are able to address an issue yourself, you are welcome to submit a pull request.
